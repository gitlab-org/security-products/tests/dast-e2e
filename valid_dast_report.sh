#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

test_report_contains_a_vulnerability() {
  grep -q 'The Server header exposes version information' gl-dast-report.json
  assert_equals "0" "$?" "Report does not include expected vulnerability"
}
